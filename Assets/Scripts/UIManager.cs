﻿using UnityEngine;
using System.Collections;
using StartApp;
public class UIManager : MonoBehaviour {

	public Animator startButton;
	public Animator settingsButton;
	public Animator dialog;

	public GameObject btnMusicOn;
	public GameObject btnMusicOff;
	public GameObject btnAudioOn;
	public GameObject btnAuidioOff;

	void Awake(){
		PlayerPrefs.SetInt ("music_on", 1);
		PlayerPrefs.SetInt ("audio_on", 1);
		PlayerPrefs.Save ();

	}

	// Use this for initialization
	void Start () {
	
		//startButton.SetBool("isHidden", false);
		//settingsButton.SetBool ("isHidden", false);


		#if UNITY_ANDROID
		StartAppWrapper.loadAd();
		#endif
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void Play(){
		Debug.Log ("Play");
		startButton.SetBool("isHidden", true);
		settingsButton.SetBool ("isHidden", true);
		Application.LoadLevel ("RocketMouse");
	}


	public void OpenSettings()
	{
		startButton.SetBool("isHidden", true);
		settingsButton.SetBool("isHidden", true);

		dialog.enabled = true;
		dialog.SetBool("isHidden", false);
	}


	public void CloseSettings()
	{
		startButton.SetBool("isHidden", false);
		settingsButton.SetBool("isHidden", false);
		dialog.SetBool("isHidden", true);

	}

	public void DisableMusic(){
		PlayerPrefs.SetInt ("music_on", 0);
		PlayerPrefs.Save ();
		btnMusicOff.SetActive (true);
		btnMusicOn.SetActive (false);

	}
	public void EnableMusic(){
		PlayerPrefs.SetInt ("music_on", 1);
		PlayerPrefs.Save ();
		btnMusicOff.SetActive (false);
		btnMusicOn.SetActive (true);
	}


	public void DisableAudio(){
		PlayerPrefs.SetInt ("audio_on", 0);
		PlayerPrefs.Save ();
		btnAuidioOff.SetActive (true);
		btnAudioOn.SetActive (false);
		
	}
	public void EnableAudio(){
		PlayerPrefs.SetInt ("audio_on", 1);
		PlayerPrefs.Save ();
		btnAuidioOff.SetActive (false);
		btnAudioOn.SetActive (true);
	}

	public void PlayGame(){

	}

}
