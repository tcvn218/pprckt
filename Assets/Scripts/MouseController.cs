﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using StartApp;
public class MouseController : MonoBehaviour {

	public float jetpackForce = 75.0f;

	public float forwardMovementSpeed = 3.0f;

	public Transform groundCheckTransform;
	
	private bool grounded;
	
	public LayerMask groundCheckLayerMask;
	
	Animator animator;

	public ParticleSystem jetpack;

	private bool dead = false;

	private uint coins = 0;


	public Texture2D coinIconTexture;

	public AudioClip coinCollectSound;

	public AudioSource jetpackAudio;
	
	public AudioSource footstepsAudio;

	public ParallaxScroll parallax;

	public Text coinsLabel;
	public Text coinsResult;
	public GameObject restartPanel;


	public Animator contentPanel;

	public Animator btnRotateSetting;

	public GameObject btnMusicOn;
	public GameObject btnMusicOff;
	public GameObject btnAudioOn;
	public GameObject btnAuidioOff;

    AdsController googleMobileAds;

	bool isPause = false; 

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();	
		restartPanel.SetActive(false);

		Debug.Log ("Current status of music :"+PlayerPrefs.GetInt ("music_on"));

		RectTransform transform = contentPanel.gameObject.transform as RectTransform;        
		Vector2 position = transform.anchoredPosition;
		position.y -= transform.rect.height;
		transform.anchoredPosition = position;


		if (PlayerPrefs.GetInt ("music_on") == 0) {
			btnMusicOff.SetActive (true);
			btnMusicOn.SetActive (false);

			footstepsAudio.enabled = false;
			jetpackAudio.enabled = false;


		}
		if (PlayerPrefs.GetInt ("audio_on") == 0) {
			btnAuidioOff.SetActive (true);
			btnAudioOn.SetActive (false);
		
		}

		GameObject gameControllerObject = GameObject.FindWithTag ("AdsObject");
		if (gameControllerObject != null) {
			googleMobileAds = gameControllerObject.GetComponent <AdsController>();
			
			//googleMobileAds.showBanner();
			//Debug.Log ("count : " + googleMobileAds.GetCount());

			if( googleMobileAds.GetCount() == 3){
				
				//googleMobileAds.showBanner();
				Debug.Log ("count : " + googleMobileAds.GetCount());

				
			}
			
			if( googleMobileAds.GetCount() % 5 == 0){
				
				#if UNITY_ANDROID
				StartAppWrapper.showAd();
				StartAppWrapper.loadAd();
				#endif

				/*
				#if UNITY_ANDROID
				StartAppWrapper.addBanner( 
				                          StartAppWrapper.BannerType.STANDARD,
				                          StartAppWrapper.BannerPosition.TOP);
				#endif
				*/
				
			}	
		}else{ 

			Debug.Log (" Can't fin the AdsObject tag");
		}
		
		
		if (gameControllerObject != null)
		{

		}
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate () 
	{
		if(!isPause){ 
			bool jetpackActive = Input.GetButton("Fire1");
			
			jetpackActive = jetpackActive && !dead;
			
			if (jetpackActive)
			{
				rigidbody2D.AddForce(new Vector2(0, jetpackForce));
			}
			
			if (!dead)
			{
				Vector2 newVelocity = rigidbody2D.velocity;
				newVelocity.x = forwardMovementSpeed;
				rigidbody2D.velocity = newVelocity;
			}
			
			UpdateGroundedStatus();
			
			AdjustJetpack(jetpackActive);
			//Debug.Log(PlayerPrefs.GetInt ("music_on"));

			if (PlayerPrefs.GetInt ("music_on") == 1) {
				AdjustFootstepsAndJetpackSound(jetpackActive);
			}else{
				footstepsAudio.enabled = false;
				jetpackAudio.enabled = false;
			}



			parallax.offset = transform.position.x;
		}
	} 

	void UpdateGroundedStatus()
	{
		//1
		grounded = Physics2D.OverlapCircle(groundCheckTransform.position, 0.1f, groundCheckLayerMask);
		
		//2
		animator.SetBool("grounded", grounded);
	}

	void AdjustJetpack (bool jetpackActive)
	{
		jetpack.enableEmission = !grounded;
		jetpack.emissionRate = jetpackActive ? 300.0f : 75.0f; 
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag("Coins"))
			CollectCoin(collider);

		else if (collider.gameObject.CompareTag("Cactus"))
			HitByCactus(collider);

		else
			HitByLaser(collider);
	}

	void HitByCactus(Collider2D laserCollider)
	{
		dead = true;
		animator.SetBool("dead", true);
	}
		
	void HitByLaser(Collider2D laserCollider)
	{
		if (!dead)
			laserCollider.gameObject.audio.Play();

		dead = true;

		animator.SetBool("dead", true);
	}

	void CollectCoin(Collider2D coinCollider)
	{
		coins++;
		
		Destroy(coinCollider.gameObject);
		coinsLabel.text = coins.ToString();
		coinsResult.text = coins.ToString ();
		if (PlayerPrefs.GetInt ("music_on") == 1) {
			AudioSource.PlayClipAtPoint(coinCollectSound, transform.position);
		}
	}

	void OnGUI()
	{


		DisplayRestartButton();
	}



	void DisplayRestartButton()
	{
		if (dead && grounded)
		{
			restartPanel.SetActive(true);
			//Destroy(this.gameObject);
			/*
			Rect buttonRect = new Rect(Screen.width * 0.35f, Screen.height * 0.45f, Screen.width * 0.30f, Screen.height * 0.1f);
			if (GUI.Button(buttonRect, "Tap to restart!"))
			{
				Application.LoadLevel (Application.loadedLevelName);
			};
			*/
		}
	}

	public void RestartGame(){
		Debug.Log ("click restart button");
		Application.LoadLevel (Application.loadedLevelName);
	}

	void AdjustFootstepsAndJetpackSound(bool jetpackActive)    
	{
		footstepsAudio.enabled = !dead && grounded;
		
		jetpackAudio.enabled =  !dead && !grounded;
		jetpackAudio.volume = jetpackActive ? 1.0f : 0.5f;        
	}



	public void ToggleMenu()
	{

		contentPanel.enabled = true;
		
		bool isHidden = contentPanel.GetBool("isHidden");
		contentPanel.SetBool("isHidden", !isHidden);

		btnRotateSetting.enabled = true;
		btnRotateSetting.SetBool("isHidden", !isHidden);

		isPause = !isHidden;

	}

	/*
	 * Back to Menu 
    */

	public void BackMenu(){ 
		Application.LoadLevel ("Menu");
	}


	public void DisableMusic(){
		PlayerPrefs.SetInt ("music_on", 0);
		PlayerPrefs.Save ();
		btnMusicOff.SetActive (true);
		btnMusicOn.SetActive (false);
		ToggleMenu ();

		
	}
	public void EnableMusic(){
		PlayerPrefs.SetInt ("music_on", 1);
		PlayerPrefs.Save ();
		btnMusicOff.SetActive (false);
		btnMusicOn.SetActive (true);
		ToggleMenu ();
	}
	
	
	public void DisableAudio(){
		PlayerPrefs.SetInt ("audio_on", 0);
		PlayerPrefs.Save ();
		btnAuidioOff.SetActive (true);
		btnAudioOn.SetActive (false);
		ToggleMenu ();
		
	}
	public void EnableAudio(){
		PlayerPrefs.SetInt ("audio_on", 1);
		PlayerPrefs.Save ();
		btnAuidioOff.SetActive (false);
		btnAudioOn.SetActive (true);
		ToggleMenu ();
	}
}
